package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CrimeSceneTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class CrimeSceneAddPresenter {

    @Inject
    DbHelper helper;


    public CrimeSceneAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String addres, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CrimeSceneTable.ADDRESS, addres);
        contentValues.put(CrimeSceneTable.ID_EXPERTISE, id);
        helper.insertItem(CrimeSceneTable.NAME_TABLE, contentValues);
    }


    public void update(String addres, String id_exp, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CrimeSceneTable.ADDRESS, addres);
        contentValues.put(CrimeSceneTable.ID_EXPERTISE, id_exp);
        helper.updateItem(CrimeSceneTable.NAME_TABLE, contentValues, CrimeSceneTable.ID,
                String.valueOf(id));
    }

}
