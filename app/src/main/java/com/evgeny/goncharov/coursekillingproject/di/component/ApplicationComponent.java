package com.evgeny.goncharov.coursekillingproject.di.component;


import com.evgeny.goncharov.coursekillingproject.di.module.ApplicationModule;
import com.evgeny.goncharov.coursekillingproject.di.module.ManagersModule;
import com.evgeny.goncharov.coursekillingproject.di.module.PresentersModule;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.activity.MainPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CluesAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CorpseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CrimeSceneAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CriminalAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CriminalCaseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.DeleteOrUpdatePresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.ExpertiseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.InterrogationAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.InvestigatorAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.UndestoodAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.WitnessAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CluesFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CorpseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CrimeSceneFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CriminalCaseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CriminalFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.ExpertiseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.InterrogationFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.InvestigatorFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.UnderstoodFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.WitnessFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.ui.activity.MainActivity;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CluesAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CorpseAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CrimeSceneAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CriminalAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CriminalCaseAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.ExpertiseAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.InterrogationAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.InvestigatorAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.UnderstoodAddDialog;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.WitnessAddDialog;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, ManagersModule.class, PresentersModule.class})
public interface ApplicationComponent {

    //activity
    void inject(MainActivity activity);

    //presenters dialog
    void inject(MainPresenter presenter);

    void inject(CriminalCaseAddPresenter presenter);

    void inject(CluesAddPresenter presenter);

    void inject(CorpseAddPresenter presenter);

    void inject(CrimeSceneAddPresenter presenter);

    void inject(ExpertiseAddPresenter presenter);

    void inject(CriminalAddPresenter presenter);

    void inject(InterrogationAddPresenter presenter);

    void inject(InvestigatorAddPresenter presenter);

    void inject(UndestoodAddPresenter presenter);

    void inject(WitnessAddPresenter presenter);

    void inject(DeleteOrUpdatePresenter presenter);

    //presenters fragment
    void inject(CriminalCaseFragmentPresenter presenter);

    void inject(CluesFragmentPresenter presenter);

    void inject(CorpseFragmentPresenter presenter);

    void inject(CrimeSceneFragmentPresenter presenter);

    void inject(CriminalFragmentPresenter presenter);

    void inject(ExpertiseFragmentPresenter presenter);

    void inject(InterrogationFragmentPresenter presenter);

    void inject(InvestigatorFragmentPresenter presenter);

    void inject(UnderstoodFragmentPresenter presenter);

    void inject(WitnessFragmentPresenter presenter);


    //fragment
    void inject(CriminalCaseAddDialog fragment);

    void inject(CluesAddDialog fragment);

    void inject(CorpseAddDialog fragment);

    void inject(CrimeSceneAddDialog fragment);

    void inject(CriminalAddDialog fragment);

    void inject(ExpertiseAddDialog fragment);

    void inject(InterrogationAddDialog fragment);

    void inject(InvestigatorAddDialog fragment);

    void inject(UnderstoodAddDialog fragment);

    void inject(WitnessAddDialog fragment);

}
