package com.evgeny.goncharov.coursekillingproject.model.view;


public class InvestigatorViewModel extends ModelView {

    private int idInvestigator;
    private String fullName;
    private String passportNumberSeries;
    private String rank;
    private String dateOfBirth;
    private int idCriminalCaseForeign;


    public int getIdInvestigator() {
        return idInvestigator;
    }

    public void setIdInvestigator(int idInvestigator) {
        this.idInvestigator = idInvestigator;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassportNumberSeries() {
        return passportNumberSeries;
    }

    public void setPassportNumberSeries(String passportNumberSeries) {
        this.passportNumberSeries = passportNumberSeries;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIdCriminalCaseForeign() {
        return idCriminalCaseForeign;
    }

    public void setIdCriminalCaseForeign(int idCriminalCaseForeign) {
        this.idCriminalCaseForeign = idCriminalCaseForeign;
    }

    @Override
    public int getId() {
        return idInvestigator;
    }
}
