package com.evgeny.goncharov.coursekillingproject.model.view;

public abstract class ModelView {
    abstract public int getId();
}
