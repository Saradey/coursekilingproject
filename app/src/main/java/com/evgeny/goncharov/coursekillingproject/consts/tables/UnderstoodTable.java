package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class UnderstoodTable {

    public static final String ID = "idUnderstood";
    public static final String TABLE_NAME = "understood";
    public static final String FULL_NAME = "fullName";
    public static final String PHONE = "phone";
    public static final String ID_INTERROGATION = "idInterro";

    public static final String CREATE_UNDERSTOOD = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FULL_NAME + " TEXT, "
            + PHONE + " TEXT, "
            + ID_INTERROGATION + " INTEGER, "
            + "FOREIGN KEY (" + ID_INTERROGATION + ") "
            + "REFERENCES " + InterrogationTable.TABLE_NAME
            + " (" + InterrogationTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
