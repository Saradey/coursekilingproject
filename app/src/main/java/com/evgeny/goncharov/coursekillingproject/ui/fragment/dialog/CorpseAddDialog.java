package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CorpseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.BaseFragmentView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CorpseAddDialog extends BaseDialogAdd {

    @BindView(R.id.fullname)
    EditText inputFullname;

    @BindView(R.id.inp_date)
    EditText inputDate;

    @BindView(R.id.die)
    EditText inputDie;

    @BindView(R.id.inp_id)
    EditText inputId;

    @Inject
    CorpseAddPresenter presenter;

    private BaseFragmentView View;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_corpse, (ViewGroup) null);
        ButterKnife.bind(this, view);

        MainApplication.getApplicationComponent().inject(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.add)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    String fullname = inputFullname.getText().toString();
                    String date = inputDate.getText().toString();
                    String die = inputDie.getText().toString();
                    String getId = inputId.getText().toString();

                    if (fullname.isEmpty() || date.isEmpty() || die.isEmpty()
                            || getId.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                    } else {
                        checkFlagInserOrUpdate();
                        View.resetList();
                    }
                }).setNegativeButton(R.string.cancel, null)
                .create();
    }


    public void checkFlagInserOrUpdate() {
        if (updateOrInsert == ApiConsts.UPDATE_ITEM_FLAG) {
            presenter.update(inputFullname.getText().toString(),
                    inputDate.getText().toString(),
                    inputDie.getText().toString(),
                    inputId.getText().toString(), updateId);
        } else if (updateOrInsert == ApiConsts.ADD_ITEM_FLAG) {
            presenter.insert(inputFullname.getText().toString(),
                    inputDate.getText().toString(),
                    inputDie.getText().toString(),
                    inputId.getText().toString());
        }
    }


    public void setCluesView(BaseFragmentView View) {
        this.View = View;
    }
}
