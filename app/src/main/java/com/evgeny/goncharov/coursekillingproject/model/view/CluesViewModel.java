package com.evgeny.goncharov.coursekillingproject.model.view;

public class CluesViewModel extends ModelView {

    private int idClues;
    private String typeOfEvidence;
    private String description;
    private int idExp;

    public int getIdClues() {
        return idClues;
    }

    public void setIdClues(int idClues) {
        this.idClues = idClues;
    }

    public String getTypeOfEvidence() {
        return typeOfEvidence;
    }

    public void setTypeOfEvidence(String typeOfEvidence) {
        this.typeOfEvidence = typeOfEvidence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdExp() {
        return idExp;
    }

    public void setIdExp(int idExp) {
        this.idExp = idExp;
    }

    @Override
    public int getId() {
        return idClues;
    }
}
