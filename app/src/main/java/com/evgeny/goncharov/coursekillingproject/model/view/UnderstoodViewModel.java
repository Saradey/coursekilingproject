package com.evgeny.goncharov.coursekillingproject.model.view;

public class UnderstoodViewModel extends ModelView {

    private int idUnderstood;
    private String fullName;
    private String phone;
    private int idInterro;

    public int getIdUnderstood() {
        return idUnderstood;
    }

    public void setIdUnderstood(int idUnderstood) {
        this.idUnderstood = idUnderstood;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIdInterro() {
        return idInterro;
    }

    public void setIdInterro(int idInterro) {
        this.idInterro = idInterro;
    }

    @Override
    public int getId() {
        return idUnderstood;
    }
}
