package com.evgeny.goncharov.coursekillingproject.model.view;


public class CorpseViewModel extends ModelView {

    private int idCorpse;
    private String fullName;
    private String dateOfBirth;
    private String causeOfDeath;
    private int idExp;


    public int getIdCorpse() {
        return idCorpse;
    }

    public void setIdCorpse(int idCorpse) {
        this.idCorpse = idCorpse;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCauseOfDeath() {
        return causeOfDeath;
    }

    public void setCauseOfDeath(String causeOfDeath) {
        this.causeOfDeath = causeOfDeath;
    }

    public int getIdExp() {
        return idExp;
    }

    public void setIdExp(int idExp) {
        this.idExp = idExp;
    }

    @Override
    public int getId() {
        return idCorpse;
    }
}
