package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CluesAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.BaseFragmentView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CluesAddDialog extends BaseDialogAdd {

    @BindView(R.id.type)
    EditText inputType;

    @BindView(R.id.description)
    EditText inputDescription;

    @BindView(R.id.inp_id)
    EditText inputId;

    @Inject
    CluesAddPresenter presenter;

    private BaseFragmentView cluesView;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_clues, (ViewGroup) null);
        ButterKnife.bind(this, view);

        MainApplication.getApplicationComponent().inject(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.add)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    String getType = inputType.getText().toString();
                    String getDescription = inputDescription.getText().toString();
                    String getId = inputId.getText().toString();

                    if (getType.isEmpty() || getDescription.isEmpty() || getId.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                    } else {
                        checkFlagInserOrUpdate();
                        cluesView.resetList();
                    }
                }).setNegativeButton(R.string.cancel, null)
                .create();
    }


    public void checkFlagInserOrUpdate() {
        if (updateOrInsert == ApiConsts.UPDATE_ITEM_FLAG) {
            presenter.update(inputType.getText().toString(),
                    inputDescription.getText().toString(),
                    inputId.getText().toString(), updateId);
        } else if (updateOrInsert == ApiConsts.ADD_ITEM_FLAG) {
            presenter.insert(inputType.getText().toString(),
                    inputDescription.getText().toString(),
                    inputId.getText().toString());
        }
    }


    public void setCluesView(BaseFragmentView cluesView) {
        this.cluesView = cluesView;
    }

}
