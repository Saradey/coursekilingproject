package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;


import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalCaseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class CriminalCaseAddPresenter {

    @Inject
    DbHelper helper;


    public CriminalCaseAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String openingDate, String description) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CriminalCaseTable.OPENING_DATE, openingDate);
        contentValues.put(CriminalCaseTable.DESCRIPTION, description);
        helper.insertItem(CriminalCaseTable.TABLE_NAME, contentValues);
    }


    public void update(String openingDate, String description, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CriminalCaseTable.OPENING_DATE, openingDate);
        contentValues.put(CriminalCaseTable.DESCRIPTION, description);
        helper.updateItem(CriminalCaseTable.TABLE_NAME, contentValues, CriminalCaseTable.ID,
                String.valueOf(id));
    }

}
