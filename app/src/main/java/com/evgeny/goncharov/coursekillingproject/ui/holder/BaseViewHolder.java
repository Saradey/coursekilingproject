package com.evgeny.goncharov.coursekillingproject.ui.holder;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.AdapterListener;
import com.evgeny.goncharov.coursekillingproject.model.view.CluesViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CorpseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CrimeSceneViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalCaseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ExpertiseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.InterrogationViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.InvestigatorViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.UnderstoodViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.WitnessViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.icon_data)
    ImageView iconItem;

    @BindView(R.id.data_text)
    TextView dataText;

    private View itemView;

    private int id = -1;
    private AdapterListener adapter;


    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.itemView = itemView;
        this.itemView.setOnClickListener(v -> {
            adapter.click(id);
        });
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(CriminalCaseViewModel item) {
        id = item.getIdCriminalCase();
        dataText.setText(itemView.getResources().getString(R.string.list_dateopen_criminalcase) +
                item.getOpeningDate() +
                "\n" +
                itemView.getResources().getString(R.string.list_desc) +
                item.getDescription() +
                "\n" +
                itemView.getResources().getString(R.string.idcriminalcase) +
                item.getIdCriminalCase());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(CluesViewModel item) {
        id = item.getIdClues();
        dataText.setText(itemView.getResources().getString(R.string.list_type_clues) +
                item.getTypeOfEvidence() +
                "\n" +
                itemView.getResources().getString(R.string.list_desc) +
                item.getDescription() +
                "\n" +
                itemView.getResources().getString(R.string.list_id_expert) +
                item.getIdExp());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(CorpseViewModel item) {
        id = item.getIdCorpse();
        dataText.setText(itemView.getResources().getString(R.string.list_name_cor) +
                item.getFullName() +
                "\n" +
                itemView.getResources().getString(R.string.list_cause) +
                item.getCauseOfDeath() +
                "\n" +
                itemView.getResources().getString(R.string.list_date) +
                item.getDateOfBirth() +
                "\n" +
                itemView.getResources().getString(R.string.list_id_expert) +
                item.getIdExp());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(CrimeSceneViewModel item) {
        id = item.getIdCrimeScene();
        dataText.setText(itemView.getResources().getString(R.string.list_address_crime) +
                item.getAddress() +
                "\n" +
                itemView.getResources().getString(R.string.list_id_expert) +
                item.getIdExp());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(CriminalViewModel item) {
        id = item.getIdCriminal();
        dataText.setText(itemView.getResources().getString(R.string.list_criminalname) +
                item.getFullName() +
                "\n" +
                itemView.getResources().getString(R.string.list_criminalcharect) +
                item.getCharacteristic() +
                "\n" +
                itemView.getResources().getString(R.string.list_idinterrogation) +
                item.getIdInterro());
    }

    @SuppressLint("SetTextI18n")
    public void bindViewHolder(ExpertiseViewModel item) {
        id = item.getIdExpertise();
        dataText.setText(itemView.getResources().getString(R.string.list_id_expert) +
                item.getIdExpertise() +
                "\n" +
                itemView.getResources().getString(R.string.list_experpinion) +
                item.getExpertPpinion() +
                "\n" +
                itemView.getResources().getString(R.string.list_experdate) +
                item.getTheDateOfThe());
    }

    @SuppressLint("SetTextI18n")
    public void bindViewHolder(InterrogationViewModel item) {
        id = item.getIdInterrogation();
        dataText.setText(itemView.getResources().getString(R.string.list_idinterrogation) +
                item.getIdInterrogation() +
                "\n" +
                itemView.getResources().getString(R.string.list_desc) +
                item.getDescription());
    }

    @SuppressLint("SetTextI18n")
    public void bindViewHolder(InvestigatorViewModel item) {
        id = item.getIdInvestigator();
        dataText.setText(itemView.getResources().getString(R.string.list_investname) +
                item.getFullName() +
                "\n" +
                itemView.getResources().getString(R.string.list_serial) +
                item.getPassportNumberSeries() +
                "\n" +
                itemView.getResources().getString(R.string.list_rank) +
                item.getRank() +
                "\n" +
                itemView.getResources().getString(R.string.list_datebith) +
                item.getDateOfBirth() +
                "\n" +
                itemView.getResources().getString(R.string.idcriminalcase) +
                item.getIdCriminalCaseForeign());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(UnderstoodViewModel item) {
        id = item.getIdUnderstood();
        dataText.setText(itemView.getResources().getString(R.string.list_understoodname) +
                item.getFullName() +
                "\n" +
                itemView.getResources().getString(R.string.list_phone) +
                item.getPhone() +
                "\n" +
                itemView.getResources().getString(R.string.list_idinterrogation) +
                item.getIdInterro());
    }


    @SuppressLint("SetTextI18n")
    public void bindViewHolder(WitnessViewModel item) {
        id = item.getIdWitness();
        dataText.setText(itemView.getResources().getString(R.string.list_witnessname) +
                item.getFullNameWitness() +
                "\n" +
                itemView.getResources().getString(R.string.list_phone) +
                item.getPhoneWitness() +
                "\n" +
                itemView.getResources().getString(R.string.list_addresswitnes) +
                item.getAddressWitness() +
                "\n" +
                itemView.getResources().getString(R.string.list_idinterrogation) +
                item.getIdInterro());
    }


    public void setAdapter(AdapterListener adapter) {
        this.adapter = adapter;
    }

}
