package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CorpseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.CorpseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CorpseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class CorpseFragmentPresenter extends MvpPresenter<CorpseView> {

    @Inject
    DbHelper dbHelper;

    public CorpseFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }


    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }


    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                CorpseTable.FULL_NAME,
                CorpseTable.DATE,
                CorpseTable.CAUSE,
                CorpseTable.ID_EXPERTISE,
                CorpseTable.ID
        };

        Cursor cursor = database.query(CorpseTable.NAME_TABLE,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private CorpseViewModel cursorTo(Cursor cursor) {
        CorpseViewModel entity = new CorpseViewModel();
        entity.setFullName(cursor.getString(0));
        entity.setDateOfBirth(cursor.getString(1));
        entity.setCauseOfDeath(cursor.getString(2));
        entity.setIdExp(cursor.getInt(3));
        entity.setIdCorpse(cursor.getInt(4));
        return entity;
    }

}
