package com.evgeny.goncharov.coursekillingproject.mvp.view.activity;

import com.evgeny.goncharov.coursekillingproject.common.adapter.AdapterListener;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;

public interface ListenerForMainActivity {

    void setClick(int id, TypeFragment typeFragment, AdapterListener adapter);

}
