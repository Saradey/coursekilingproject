package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CriminalCaseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.BaseFragmentView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;



public class CriminalCaseAddDialog extends BaseDialogAdd {

    @BindView(R.id.opening_date)
    EditText inputDate;

    @BindView(R.id.description)
    EditText inputDescription;

    @Inject
    CriminalCaseAddPresenter presenter;

    private BaseFragmentView View;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_criminalcase, (ViewGroup) null);
        ButterKnife.bind(this, view);

        MainApplication.getApplicationComponent().inject(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.add)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    String getDate = inputDate.getText().toString();
                    String getDesc = inputDescription.getText().toString();
                    if (getDate.isEmpty() || getDesc.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                    } else {
                        checkFlagInserOrUpdate();
                        View.resetList();
                    }
                }).setNegativeButton(R.string.cancel, null)
                .create();
    }

    private void checkFlagInserOrUpdate() {
        if (updateOrInsert == ApiConsts.UPDATE_ITEM_FLAG) {
            presenter.update(inputDate.getText().toString(),
                    inputDescription.getText().toString(), updateId);
        } else if (updateOrInsert == ApiConsts.ADD_ITEM_FLAG) {
            presenter.insert(inputDate.getText().toString(),
                    inputDescription.getText().toString());
        }
    }

    public void setView(BaseFragmentView view) {
        this.View = view;
    }
}
