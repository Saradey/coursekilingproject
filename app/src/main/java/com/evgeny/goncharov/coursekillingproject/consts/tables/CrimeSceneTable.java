package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class CrimeSceneTable {

    public static final String NAME_TABLE = "CrimeScene";
    public static final String ID = "idCrimeScene";
    public static final String ADDRESS = "address";
    public static final String ID_EXPERTISE = "idExp";

    public static final String CREATE_SCENE = "CREATE TABLE " + NAME_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ADDRESS + " TEXT, "
            + ID_EXPERTISE + " INTEGER, "
            + "FOREIGN KEY (" + ID_EXPERTISE + ") "
            + "REFERENCES " + ExpertiseTable.TABLE_NAME
            + " (" + ExpertiseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
