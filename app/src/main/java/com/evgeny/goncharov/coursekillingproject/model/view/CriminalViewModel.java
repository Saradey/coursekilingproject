package com.evgeny.goncharov.coursekillingproject.model.view;

public class CriminalViewModel extends ModelView {

    private int idCriminal;
    private String fullName;
    private String characteristic;
    private int idInterro;


    public int getIdCriminal() {
        return idCriminal;
    }

    public void setIdCriminal(int idCriminal) {
        this.idCriminal = idCriminal;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(String characteristic) {
        this.characteristic = characteristic;
    }

    public int getIdInterro() {
        return idInterro;
    }

    public void setIdInterro(int idInterro) {
        this.idInterro = idInterro;
    }

    @Override
    public int getId() {
        return idCriminal;
    }
}
