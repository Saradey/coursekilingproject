package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CriminalView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class CriminalFragmentPresenter extends MvpPresenter<CriminalView> {

    @Inject
    DbHelper dbHelper;

    public CriminalFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }


    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                CriminalTable.FULL_NAME,
                CriminalTable.CHARECT,
                CriminalTable.ID_INTERROGATION,
                CriminalTable.ID
        };

        Cursor cursor = database.query(CriminalTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private CriminalViewModel cursorTo(Cursor cursor) {
        CriminalViewModel entity = new CriminalViewModel();
        entity.setFullName(cursor.getString(0));
        entity.setCharacteristic(cursor.getString(1));
        entity.setIdInterro(cursor.getInt(2));
        entity.setIdCriminal(cursor.getInt(3));
        return entity;
    }
}
