package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.ExpertiseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.ExpertiseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.ExpertiseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class ExpertiseFragmentPresenter extends MvpPresenter<ExpertiseView> {

    @Inject
    DbHelper dbHelper;

    public ExpertiseFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                ExpertiseTable.ID,
                ExpertiseTable.DATE,
                ExpertiseTable.EXPERT,
        };

        Cursor cursor = database.query(ExpertiseTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private ExpertiseViewModel cursorTo(Cursor cursor) {
        ExpertiseViewModel entity = new ExpertiseViewModel();
        entity.setIdExpertise(cursor.getInt(0));
        entity.setTheDateOfThe(cursor.getString(1));
        entity.setExpertPpinion(cursor.getString(2));
        return entity;
    }


}
