package com.evgeny.goncharov.coursekillingproject.di.module;


import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CluesAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CorpseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CrimeSceneAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CriminalAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.CriminalCaseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.ExpertiseAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.InterrogationAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.InvestigatorAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.UndestoodAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.WitnessAddPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresentersModule {

    @Singleton
    @Provides
    CriminalCaseAddPresenter provideAddCriminalCasePresenter() {
        return new CriminalCaseAddPresenter();
    }

    @Singleton
    @Provides
    CluesAddPresenter provideCluesAddPresenter() {
        return new CluesAddPresenter();
    }

    @Singleton
    @Provides
    CorpseAddPresenter provideCorpseAddPresenter() {
        return new CorpseAddPresenter();
    }

    @Singleton
    @Provides
    CrimeSceneAddPresenter provideCrimeSceneAddPresenter() {
        return new CrimeSceneAddPresenter();
    }

    @Singleton
    @Provides
    CriminalAddPresenter provideCriminalAddPresenter() {
        return new CriminalAddPresenter();
    }

    @Singleton
    @Provides
    ExpertiseAddPresenter provideExpertiseAddPresenter() {
        return new ExpertiseAddPresenter();
    }

    @Singleton
    @Provides
    InterrogationAddPresenter provideInterrogationAddPresenter() {
        return new InterrogationAddPresenter();
    }

    @Singleton
    @Provides
    InvestigatorAddPresenter provideInvestigatorAddPresenter() {
        return new InvestigatorAddPresenter();
    }

    @Singleton
    @Provides
    UndestoodAddPresenter provideUndestoodAddPresenter() {
        return new UndestoodAddPresenter();
    }

    @Singleton
    @Provides
    WitnessAddPresenter provideWitnessAddPresenter() {
        return new WitnessAddPresenter();
    }

}
