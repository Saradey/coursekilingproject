package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CrimeSceneTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.CrimeSceneViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CrimeSceneView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class CrimeSceneFragmentPresenter extends MvpPresenter<CrimeSceneView> {

    @Inject
    DbHelper dbHelper;

    public CrimeSceneFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }


    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                CrimeSceneTable.ADDRESS,
                CrimeSceneTable.ID_EXPERTISE,
                CrimeSceneTable.ID
        };

        Cursor cursor = database.query(CrimeSceneTable.NAME_TABLE,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private CrimeSceneViewModel cursorTo(Cursor cursor) {
        CrimeSceneViewModel entity = new CrimeSceneViewModel();
        entity.setAddress(cursor.getString(0));
        entity.setIdExp(cursor.getInt(1));
        entity.setIdCrimeScene(cursor.getInt(2));
        return entity;
    }

}
