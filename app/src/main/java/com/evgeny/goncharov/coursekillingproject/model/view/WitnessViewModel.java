package com.evgeny.goncharov.coursekillingproject.model.view;

public class WitnessViewModel extends ModelView {

    private int idWitness;
    private String fullNameWitness;
    private String phoneWitness;
    private String addressWitness;
    private int idInterro;


    public int getIdWitness() {
        return idWitness;
    }

    public void setIdWitness(int idWitness) {
        this.idWitness = idWitness;
    }

    public String getFullNameWitness() {
        return fullNameWitness;
    }

    public void setFullNameWitness(String fullNameWitness) {
        this.fullNameWitness = fullNameWitness;
    }

    public String getPhoneWitness() {
        return phoneWitness;
    }

    public void setPhoneWitness(String phoneWitness) {
        this.phoneWitness = phoneWitness;
    }

    public String getAddressWitness() {
        return addressWitness;
    }

    public void setAddressWitness(String addressWitness) {
        this.addressWitness = addressWitness;
    }

    public int getIdInterro() {
        return idInterro;
    }

    public void setIdInterro(int idInterro) {
        this.idInterro = idInterro;
    }

    @Override
    public int getId() {
        return idWitness;
    }
}
