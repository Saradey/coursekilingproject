package com.evgeny.goncharov.coursekillingproject.ui.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.AdapterListener;
import com.evgeny.goncharov.coursekillingproject.common.managers.MyFragmentManager;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.activity.MainPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.MainView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.BaseFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.CluesFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.CorpseFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.CrimeSceneFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.CriminalCaseFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.CriminalFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.ExpertiseFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.InterrogationFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.InvestigatorFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.UnderstoodFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.WitnessFragment;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.DeleteOrUpdateDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity
        implements MainView, ListenerForMainActivity {

    @Inject
    MyFragmentManager myFragmentManager;

    @InjectPresenter
    MainPresenter presenter;

    @BindView(R.id.my_toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_menu)
    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        setActionBar();
        setListenerToNavigation();

        MainApplication.getApplicationComponent().inject(this);

        goToTheCrimalCaseFragment();
    }


    private void setActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.dehaze);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }
    }


    private void setListenerToNavigation() {
        navigationView.setNavigationItemSelectedListener(menuItem -> {
            menuItem.setChecked(true);
            drawerLayout.closeDrawers();

            switch (menuItem.getItemId()) {

                case R.id.btn_crimalcase:
                    presenter.goToTheCrimalCaseFragment();
                    break;

                case R.id.btn_investigator:
                    presenter.goToTheInvestigatorFragment();
                    break;

                case R.id.btn_expertise:
                    presenter.goToTheExpertiseFragment();
                    break;

                case R.id.btn_corpse:
                    presenter.goToTheCorpseFragment();
                    break;

                case R.id.btn_clues:
                    presenter.goToTheCluesFragment();
                    break;

                case R.id.btn_crimescene:
                    presenter.goToTheCrimeSceneFragment();
                    break;

                case R.id.btn_interrogation:
                    presenter.goToTheInterrogationFragment();
                    break;

                case R.id.btn_understood:
                    presenter.goToTheUnderstood();
                    break;

                case R.id.btn_criminal:
                    presenter.goToTheCriminal();
                    break;

                case R.id.btn_witness:
                    presenter.goToTheWitness();
                    break;

                default:
                    break;
            }
            return true;
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void goToTheCrimalCaseFragment() {
        CriminalCaseFragment criminalCaseFragment = new CriminalCaseFragment();
        myFragmentManager.setFragment(this, criminalCaseFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheInvestigatorFragment() {
        InvestigatorFragment investigatorFragment = new InvestigatorFragment();
        myFragmentManager.setFragment(this, investigatorFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheExpertiseFragment() {
        ExpertiseFragment expertiseFragment = new ExpertiseFragment();
        myFragmentManager.setFragment(this, expertiseFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheCorpseFragment() {
        CorpseFragment corpseFragment = new CorpseFragment();
        myFragmentManager.setFragment(this, corpseFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheCluesFragment() {
        CluesFragment cluesFragment = new CluesFragment();
        myFragmentManager.setFragment(this, cluesFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheCrimeSceneFragment() {
        CrimeSceneFragment crimeSceneFragment = new CrimeSceneFragment();
        myFragmentManager.setFragment(this, crimeSceneFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheInterrogationFragment() {
        InterrogationFragment interrogationFragment = new InterrogationFragment();
        myFragmentManager.setFragment(this, interrogationFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheUnderstood() {
        UnderstoodFragment understoodFragment = new UnderstoodFragment();
        myFragmentManager.setFragment(this, understoodFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheCriminal() {
        CriminalFragment criminalFragment = new CriminalFragment();
        myFragmentManager.setFragment(this, criminalFragment, R.id.space_for_fragment);
    }

    @Override
    public void goToTheWitness() {
        WitnessFragment witnessFragment = new WitnessFragment();
        myFragmentManager.setFragment(this, witnessFragment, R.id.space_for_fragment);
    }

    @Override
    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
    }


    public void fragmentOnScreen(BaseFragment baseFragment) {
        setToolbarTitle(baseFragment.createToolbarTitle(this));
    }


    private void setToolbarTitle(String title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }


    @Override
    public void setClick(int id, TypeFragment typeFragment, AdapterListener adapter) {
        DeleteOrUpdateDialog dialog = new DeleteOrUpdateDialog();
        dialog.setBundle(id, typeFragment, adapter);
        dialog.setMyFragment(myFragmentManager.getFragment());
        dialog.show(getSupportFragmentManager(), ApiConsts.DIALOG_UPDATE_OR_DELETE);
    }

    @Override
    public void createAddDialogMenu() {
        myFragmentManager.createAddDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                presenter.clickButtonHome();
                return true;

            case R.id.add_item:
                presenter.pressButtonAddItem();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
