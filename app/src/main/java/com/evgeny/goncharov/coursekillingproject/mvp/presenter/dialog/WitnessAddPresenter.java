package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.WitnessTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class WitnessAddPresenter {

    @Inject
    DbHelper helper;


    public WitnessAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String fullname, String id, String phone, String address) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WitnessTable.FULL_NAME, fullname);
        contentValues.put(WitnessTable.ID_INTERROGATION, id);
        contentValues.put(WitnessTable.PHONE, phone);
        contentValues.put(WitnessTable.ADDRESS, address);
        helper.insertItem(WitnessTable.NAME_TABLE, contentValues);
    }


    public void update(String fullname, String idinter, String phone, String address, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(WitnessTable.FULL_NAME, fullname);
        contentValues.put(WitnessTable.ID_INTERROGATION, idinter);
        contentValues.put(WitnessTable.PHONE, phone);
        contentValues.put(WitnessTable.ADDRESS, address);
        helper.updateItem(WitnessTable.NAME_TABLE, contentValues, WitnessTable.ID,
                String.valueOf(id));
    }


}
