package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.ExpertiseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.ExpertiseView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.ExpertiseAddDialog;

public class ExpertiseFragment extends BaseFragment implements ExpertiseView {

    @InjectPresenter
    ExpertiseFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.expertise;
    }

    @Override
    public void createAddMenu() {
        ExpertiseAddDialog expertiseAddDialog = new ExpertiseAddDialog();
        expertiseAddDialog.setView(this);
        expertiseAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        expertiseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Expertise);
    }

    @Override
    public void createUpdateMenu(int id) {
        ExpertiseAddDialog expertiseAddDialog = new ExpertiseAddDialog();
        expertiseAddDialog.setView(this);
        expertiseAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        expertiseAddDialog.setUpdateId(id);
        expertiseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Expertise);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Expertise, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
