package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalCaseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalCaseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CriminalCaseView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class CriminalCaseFragmentPresenter extends MvpPresenter<CriminalCaseView> {

    @Inject
    DbHelper dbHelper;

    public CriminalCaseFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }


    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }


    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                CriminalCaseTable.ID,
                CriminalCaseTable.DESCRIPTION,
                CriminalCaseTable.OPENING_DATE,
        };

        Cursor cursor = database.query(CriminalCaseTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private CriminalCaseViewModel cursorTo(Cursor cursor) {
        CriminalCaseViewModel entity = new CriminalCaseViewModel();
        entity.setIdCriminalCase(cursor.getInt(0));
        entity.setDescription(cursor.getString(1));
        entity.setOpeningDate(cursor.getString(2));
        return entity;
    }

}
