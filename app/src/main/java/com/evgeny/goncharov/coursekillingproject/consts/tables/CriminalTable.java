package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class CriminalTable {

    public static final String TABLE_NAME = "CriminalTable";
    public static final String ID = "idCriminal";
    public static final String FULL_NAME = "fullName";
    public static final String CHARECT = "characteristic";
    public static final String ID_INTERROGATION = "idInterro";

    public static final String CREATE_CRIMINAL = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FULL_NAME + " TEXT, "
            + CHARECT + " TEXT, "
            + ID_INTERROGATION + " INTEGER, "
            + "FOREIGN KEY (" + ID_INTERROGATION + ") "
            + "REFERENCES " + InterrogationTable.TABLE_NAME
            + " (" + InterrogationTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
