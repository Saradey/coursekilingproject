package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.UnderstoodFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.UnderstoodView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.UnderstoodAddDialog;

public class UnderstoodFragment extends BaseFragment implements UnderstoodView {


    @InjectPresenter
    UnderstoodFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.understood;
    }


    @Override
    public void createAddMenu() {
        UnderstoodAddDialog understoodAddDialog = new UnderstoodAddDialog();
        understoodAddDialog.setView(this);
        understoodAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        understoodAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Understood);
    }

    @Override
    public void createUpdateMenu(int id) {
        UnderstoodAddDialog understoodAddDialog = new UnderstoodAddDialog();
        understoodAddDialog.setView(this);
        understoodAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        understoodAddDialog.setUpdateId(id);
        understoodAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Understood);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Understood, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
