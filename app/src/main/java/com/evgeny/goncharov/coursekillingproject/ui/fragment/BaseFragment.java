package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.BaseFragmentView;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseFragmentView {

    @BindView(R.id.main_list)
    RecyclerView mainList;

    protected BaseAdapter adapter;

    private ListenerForMainActivity listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_information_list, container, false);
        ButterKnife.bind(this, view);
        inflateList();
        return view;
    }

    //для отображения заголовка тулбара +
    public String createToolbarTitle(Context context) {
        return context.getString(getTitleFragment());
    }

    @StringRes
    abstract protected int getTitleFragment();

    abstract protected void inflateList();

    abstract public void createAddMenu();

    abstract public void createUpdateMenu(int id);

    public void setListener(ListenerForMainActivity listener) {
        this.listener = listener;
    }

    @Override
    public void resetList() {
        inflateList();
    }
}
