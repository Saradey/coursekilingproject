package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class ExpertiseTable {

    public static final String TABLE_NAME = "Expertise";
    public static final String ID = "idExpertise";
    public static final String DATE = "theDateOfThe";
    public static final String EXPERT = "expertPpinion";
    public static final String ID_CRIMINAL_CASE = "idCriminal";


    public static final String CREATE_EXPERTISE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EXPERT + " TEXT, "
            + DATE + " TEXT, "
            + ID_CRIMINAL_CASE + " INTEGER, "
            + "FOREIGN KEY (" + ID_CRIMINAL_CASE + ") "
            + "REFERENCES " + CriminalCaseTable.TABLE_NAME
            + " (" + CriminalCaseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
