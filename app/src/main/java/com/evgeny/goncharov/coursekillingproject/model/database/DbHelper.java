package com.evgeny.goncharov.coursekillingproject.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.evgeny.goncharov.coursekillingproject.consts.tables.CluesTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CorpseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CrimeSceneTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalCaseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.ExpertiseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InterrogationTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InvestigatorTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.UnderstoodTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.WitnessTable;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db_member";    //имя файла
    private static final int DATABASE_VERSION = 1;

    //создаем
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //создаем таблицу
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CriminalCaseTable.CREATE_CRIMINAL_CASE);
        db.execSQL(InterrogationTable.CREATE_INTERROGATION);
        db.execSQL(ExpertiseTable.CREATE_EXPERTISE);
        db.execSQL(UnderstoodTable.CREATE_UNDERSTOOD);
        db.execSQL(CriminalTable.CREATE_CRIMINAL);
        db.execSQL(WitnessTable.CREATE_WITNESS);
        db.execSQL(CorpseTable.CREATE_CORPSE);
        db.execSQL(CrimeSceneTable.CREATE_SCENE);
        db.execSQL(CluesTable.CREATE_CLUES);
        db.execSQL(InvestigatorTable.CREATE_INVESTIGATOR);
    }


    //если версия поменялась
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    public void insertItem(String tableName, ContentValues values) {
        SQLiteDatabase dp = getWritableDatabase();
        dp.insert(tableName, null, values);
    }


    public void updateItem(String tableName, ContentValues values, String where, String id) {
        SQLiteDatabase dp = getWritableDatabase();
        dp.update(tableName, values, where + " =?", new String[]{id});
    }


    public void deleteItem(String tableName, String where, String id) {
        SQLiteDatabase dp = getWritableDatabase();
        dp.delete(tableName, where + " =?", new String[]{id});
    }

}
