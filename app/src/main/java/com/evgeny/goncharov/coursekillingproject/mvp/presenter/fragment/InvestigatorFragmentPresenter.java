package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InvestigatorTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.InvestigatorViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.InvestigatorView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class InvestigatorFragmentPresenter extends MvpPresenter<InvestigatorView> {

    @Inject
    DbHelper dbHelper;

    public InvestigatorFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                InvestigatorTable.FULL_NAME,
                InvestigatorTable.PASSPORT,
                InvestigatorTable.RANK,
                InvestigatorTable.DATE,
                InvestigatorTable.ID_CRIMINAL_CASE,
                InvestigatorTable.ID,
        };

        Cursor cursor = database.query(InvestigatorTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private InvestigatorViewModel cursorTo(Cursor cursor) {
        InvestigatorViewModel entity = new InvestigatorViewModel();
        entity.setFullName(cursor.getString(0));
        entity.setPassportNumberSeries(cursor.getString(1));
        entity.setRank(cursor.getString(2));
        entity.setDateOfBirth(cursor.getString(3));
        entity.setIdCriminalCaseForeign(cursor.getInt(4));
        entity.setIdInvestigator(cursor.getInt(5));
        return entity;
    }

}
