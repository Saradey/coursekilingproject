package com.evgeny.goncharov.coursekillingproject.common.managers;


import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;

import com.evgeny.goncharov.coursekillingproject.ui.activity.MainActivity;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.BaseFragment;

public class MyFragmentManager {

    private BaseFragment baseFragment;

    public void setFragment(MainActivity mainActivity, BaseFragment baseFragment, @IdRes int container) {
        baseFragment.setListener(mainActivity);
        FragmentTransaction transaction = mainActivity
                .getSupportFragmentManager()
                .beginTransaction();
        transaction.replace(container, baseFragment);
        transaction.commit();

        setVariableFragment(baseFragment);
        setTitleToolbar(baseFragment, mainActivity);
    }

    public void createAddDialog() {
        if (baseFragment != null) {
            baseFragment.createAddMenu();
        }

    }

    private void setTitleToolbar(BaseFragment baseFragment, MainActivity mainActivity) {
        mainActivity.fragmentOnScreen(baseFragment);
    }

    private void setVariableFragment(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }


    public BaseFragment getFragment() {
        return baseFragment;
    }


}
