package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InterrogationTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class InterrogationAddPresenter {

    @Inject
    DbHelper helper;


    public InterrogationAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String desc, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(InterrogationTable.ID_CRIMINAL_CASE, id);
        contentValues.put(InterrogationTable.DESCRIPTION, desc);
        helper.insertItem(InterrogationTable.TABLE_NAME, contentValues);
    }


    public void update(String desc, String idInter, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(InterrogationTable.ID_CRIMINAL_CASE, idInter);
        contentValues.put(InterrogationTable.DESCRIPTION, desc);
        helper.updateItem(InterrogationTable.TABLE_NAME, contentValues, InterrogationTable.ID,
                String.valueOf(id));
    }

}
