package com.evgeny.goncharov.coursekillingproject.consts;

/**
 * @author Evgeny Goncharov
 */

public class ApiConsts {

    public static final String CriminalCase = "CriminalCase";

    public static final String Investigator = "Investigator";

    public static final String Corpse = "Corpse";

    public static final String Expertise = "Expertise";

    public static final String Clues = "CluesView";

    public static final String CrimeScene = "CrimeScene";

    public static final String Interrogation = "Interrogation";

    public static final String Understood = "Understood";

    public static final String Criminal = "Criminal";

    public static final String Witness = "Witness";

    public static final String KEY_BUNDLE_FOR_DIALOG = "key_dialog";

    public static final int UPDATE_ITEM_FLAG = 1;

    public static final int ADD_ITEM_FLAG = 2;

    public static final String DIALOG_UPDATE_OR_DELETE = "update_or_insert";

}
