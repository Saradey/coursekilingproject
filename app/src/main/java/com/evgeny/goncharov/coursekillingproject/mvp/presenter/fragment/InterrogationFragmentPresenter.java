package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InterrogationTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.InterrogationViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.InterrogationView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class InterrogationFragmentPresenter extends MvpPresenter<InterrogationView> {

    @Inject
    DbHelper dbHelper;

    public InterrogationFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                InterrogationTable.ID,
                InterrogationTable.DESCRIPTION
        };

        Cursor cursor = database.query(InterrogationTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private InterrogationViewModel cursorTo(Cursor cursor) {
        InterrogationViewModel entity = new InterrogationViewModel();
        entity.setIdInterrogation(cursor.getInt(0));
        entity.setDescription(cursor.getString(1));
        return entity;
    }

}
