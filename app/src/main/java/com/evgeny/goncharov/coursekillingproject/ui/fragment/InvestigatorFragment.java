package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.InvestigatorFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.InvestigatorView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.InvestigatorAddDialog;

public class InvestigatorFragment extends BaseFragment implements InvestigatorView {

    @InjectPresenter
    InvestigatorFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.investigator;
    }

    @Override
    public void createAddMenu() {
        InvestigatorAddDialog investigatorAddDialog = new InvestigatorAddDialog();
        investigatorAddDialog.setView(this);
        investigatorAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        investigatorAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Investigator);
    }

    @Override
    public void createUpdateMenu(int id) {
        InvestigatorAddDialog investigatorAddDialog = new InvestigatorAddDialog();
        investigatorAddDialog.setView(this);
        investigatorAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        investigatorAddDialog.setUpdateId(id);
        investigatorAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Investigator);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Investigator, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}