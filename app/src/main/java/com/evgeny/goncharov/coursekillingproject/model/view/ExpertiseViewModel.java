package com.evgeny.goncharov.coursekillingproject.model.view;

public class ExpertiseViewModel extends ModelView {

    private int idExpertise;
    private String theDateOfThe;
    private String expertPpinion;
    private int idCriminal;


    public int getIdExpertise() {
        return idExpertise;
    }

    public void setIdExpertise(int idExpertise) {
        this.idExpertise = idExpertise;
    }

    public String getTheDateOfThe() {
        return theDateOfThe;
    }

    public void setTheDateOfThe(String theDateOfThe) {
        this.theDateOfThe = theDateOfThe;
    }

    public String getExpertPpinion() {
        return expertPpinion;
    }

    public void setExpertPpinion(String expertPpinion) {
        this.expertPpinion = expertPpinion;
    }

    public int getIdCriminal() {
        return idCriminal;
    }

    public void setIdCriminal(int idCriminal) {
        this.idCriminal = idCriminal;
    }

    @Override
    public int getId() {
        return idExpertise;
    }
}
