package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.UnderstoodTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.model.view.UnderstoodViewModel;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.UnderstoodView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class UnderstoodFragmentPresenter extends MvpPresenter<UnderstoodView> {


    @Inject
    DbHelper dbHelper;

    public UnderstoodFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                UnderstoodTable.FULL_NAME,
                UnderstoodTable.PHONE,
                UnderstoodTable.ID_INTERROGATION,
                UnderstoodTable.ID
        };

        Cursor cursor = database.query(UnderstoodTable.TABLE_NAME,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private UnderstoodViewModel cursorTo(Cursor cursor) {
        UnderstoodViewModel entity = new UnderstoodViewModel();
        entity.setFullName(cursor.getString(0));
        entity.setPhone(cursor.getString(1));
        entity.setIdInterro(cursor.getInt(2));
        entity.setIdUnderstood(cursor.getInt(3));
        return entity;
    }

}
