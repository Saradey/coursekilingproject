package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.WitnessTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.model.view.WitnessViewModel;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.WitnessView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class WitnessFragmentPresenter extends MvpPresenter<WitnessView> {


    @Inject
    DbHelper dbHelper;

    public WitnessFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }

    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                WitnessTable.FULL_NAME,
                WitnessTable.PHONE,
                WitnessTable.ID_INTERROGATION,
                WitnessTable.ADDRESS,
                WitnessTable.ID
        };

        Cursor cursor = database.query(WitnessTable.NAME_TABLE,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private WitnessViewModel cursorTo(Cursor cursor) {
        WitnessViewModel entity = new WitnessViewModel();
        entity.setFullNameWitness(cursor.getString(0));
        entity.setPhoneWitness(cursor.getString(1));
        entity.setIdInterro(cursor.getInt(2));
        entity.setAddressWitness(cursor.getString(3));
        entity.setIdWitness(cursor.getInt(4));
        return entity;
    }

}
