package com.evgeny.goncharov.coursekillingproject.consts;

public enum TypeFragment {
    CriminalCase,
    Investigator,
    Expertise,
    Corpse,
    Clues,
    CrimeScene,
    Interrogation,
    Understood,
    Criminal,
    Witness
}
