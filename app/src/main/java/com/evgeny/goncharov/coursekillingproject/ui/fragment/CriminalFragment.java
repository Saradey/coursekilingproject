package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CriminalFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CriminalView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CriminalAddDialog;

public class CriminalFragment extends BaseFragment implements CriminalView {

    @InjectPresenter
    CriminalFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.criminal;
    }

    @Override
    public void createAddMenu() {
        CriminalAddDialog criminalAddDialog = new CriminalAddDialog();
        criminalAddDialog.setView(this);
        criminalAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        criminalAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Criminal);
    }


    @Override
    public void createUpdateMenu(int id) {
        CriminalAddDialog criminalAddDialog = new CriminalAddDialog();
        criminalAddDialog.setView(this);
        criminalAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        criminalAddDialog.setUpdateId(id);
        criminalAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Criminal);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Criminal, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
