package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class CluesTable {

    public static final String NAME_TABLE = "CluesTable";
    public static final String ID = "idClues";
    public static final String TYPE = "typeOfEvidence";
    public static final String DESCRIPTION = "description";
    public static final String ID_EXPERTISE = "idExp";

    public static final String CREATE_CLUES = "CREATE TABLE " + NAME_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TYPE + " TEXT, "
            + DESCRIPTION + " TEXT, "
            + ID_EXPERTISE + " INTEGER, "
            + "FOREIGN KEY (" + ID_EXPERTISE + ") "
            + "REFERENCES " + ExpertiseTable.TABLE_NAME
            + " (" + ExpertiseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
