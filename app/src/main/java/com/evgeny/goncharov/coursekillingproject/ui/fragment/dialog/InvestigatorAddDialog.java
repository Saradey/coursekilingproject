package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.InvestigatorAddPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.BaseFragmentView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InvestigatorAddDialog extends BaseDialogAdd {

    @BindView(R.id.inp_date)
    EditText inputDate;

    @BindView(R.id.fullname)
    EditText fullName;

    @BindView(R.id.serial)
    EditText inputSerial;

    @BindView(R.id.rank)
    EditText inputRank;

    @BindView(R.id.inp_id)
    EditText inputId;

    @Inject
    InvestigatorAddPresenter presenter;

    private BaseFragmentView View;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.dialog_add_investigator, (ViewGroup) null);
        ButterKnife.bind(this, view);

        MainApplication.getApplicationComponent().inject(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.add)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    String getDate = inputDate.getText().toString();
                    String getName = fullName.getText().toString();
                    String getSerial = inputSerial.getText().toString();
                    String getRank = inputRank.getText().toString();
                    String getId = inputId.getText().toString();
                    if (getDate.isEmpty() || getName.isEmpty() || getId.isEmpty()
                            || getSerial.isEmpty() || getRank.isEmpty()) {
                        Toast.makeText(getActivity(), R.string.empty, Toast.LENGTH_SHORT).show();
                    } else {
                        checkFlagInserOrUpdate();
                        View.resetList();
                    }
                }).setNegativeButton(R.string.cancel, null)
                .create();
    }



    private void checkFlagInserOrUpdate() {
        if (updateOrInsert == ApiConsts.UPDATE_ITEM_FLAG) {
            presenter.update(inputDate.getText().toString(),
                    fullName.getText().toString(),
                    inputSerial.getText().toString(),
                    inputRank.getText().toString(),
                    inputId.getText().toString(), updateId);
        } else if (updateOrInsert == ApiConsts.ADD_ITEM_FLAG) {
            presenter.insert(inputDate.getText().toString(),
                    fullName.getText().toString(),
                    inputSerial.getText().toString(),
                    inputRank.getText().toString(),
                    inputId.getText().toString());
        }
    }


    public void setView(BaseFragmentView view) {
        this.View = view;
    }
}