package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CorpseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CorpseView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CorpseAddDialog;

public class CorpseFragment extends BaseFragment implements CorpseView {

    @InjectPresenter
    CorpseFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.corpse;
    }

    @Override
    public void createAddMenu() {
        CorpseAddDialog corpseAddDialog = new CorpseAddDialog();
        corpseAddDialog.setCluesView(this);
        corpseAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        corpseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Corpse);
    }


    @Override
    public void createUpdateMenu(int id) {
        CorpseAddDialog corpseAddDialog = new CorpseAddDialog();
        corpseAddDialog.setCluesView(this);
        corpseAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        corpseAddDialog.setUpdateId(id);
        corpseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Corpse);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Corpse, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
