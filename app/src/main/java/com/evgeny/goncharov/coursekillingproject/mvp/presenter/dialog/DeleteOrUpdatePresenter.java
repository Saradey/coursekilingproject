package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CluesTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CorpseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CrimeSceneTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalCaseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.ExpertiseTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InterrogationTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InvestigatorTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.UnderstoodTable;
import com.evgeny.goncharov.coursekillingproject.consts.tables.WitnessTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.mvp.view.dialogs.DeleteOrUpdateView;

import javax.inject.Inject;

public class DeleteOrUpdatePresenter {

    @Inject
    DbHelper helper;

    private DeleteOrUpdateView view;

    public DeleteOrUpdatePresenter(DeleteOrUpdateView view) {
        this.view = view;
        MainApplication.getApplicationComponent().inject(this);
    }


    public void update(int idItems, TypeFragment typeFragment) {
        view.createUpdateDialog();
    }


    public void delete(int idItems, TypeFragment typeFragment) {

        switch (typeFragment) {
            case Clues:
                helper.deleteItem(CluesTable.NAME_TABLE, CluesTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Corpse:
                helper.deleteItem(CorpseTable.NAME_TABLE, CorpseTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Witness:
                helper.deleteItem(WitnessTable.NAME_TABLE, WitnessTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Criminal:
                helper.deleteItem(CriminalTable.TABLE_NAME, CriminalTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Expertise:
                helper.deleteItem(ExpertiseTable.TABLE_NAME, ExpertiseTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case CrimeScene:
                helper.deleteItem(CrimeSceneTable.NAME_TABLE, CrimeSceneTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Understood:
                helper.deleteItem(UnderstoodTable.TABLE_NAME, UnderstoodTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case CriminalCase:
                helper.deleteItem(CriminalCaseTable.TABLE_NAME, CriminalCaseTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Investigator:
                helper.deleteItem(InvestigatorTable.TABLE_NAME, InvestigatorTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;

            case Interrogation:
                helper.deleteItem(InterrogationTable.TABLE_NAME, InterrogationTable.ID, String.valueOf(idItems));
                view.notifyDataDelete(idItems);
                break;
        }

    }


}
