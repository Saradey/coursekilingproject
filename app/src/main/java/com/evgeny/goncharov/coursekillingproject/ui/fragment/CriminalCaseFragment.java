package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CriminalCaseFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CriminalCaseView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CriminalCaseAddDialog;

public class CriminalCaseFragment extends BaseFragment implements CriminalCaseView {

    @InjectPresenter
    CriminalCaseFragmentPresenter presenter;


    @Override
    public int getTitleFragment() {
        return R.string.itm_crimalcase;
    }


    @Override
    public void createAddMenu() {
        CriminalCaseAddDialog criminalCaseAddDialog = new CriminalCaseAddDialog();
        criminalCaseAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        criminalCaseAddDialog.setView(this);
        criminalCaseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.CriminalCase);
    }

    @Override
    public void createUpdateMenu(int id) {
        CriminalCaseAddDialog criminalCaseAddDialog = new CriminalCaseAddDialog();
        criminalCaseAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        criminalCaseAddDialog.setView(this);
        criminalCaseAddDialog.setUpdateId(id);
        criminalCaseAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.CriminalCase);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.CriminalCase, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }

}
