package com.evgeny.goncharov.coursekillingproject.mvp.view.dialogs;

public interface DeleteOrUpdateView {
    void notifyDataDelete(int idItems);

    void createUpdateDialog();

}
