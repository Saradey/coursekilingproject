package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.WitnessFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.WitnessView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.WitnessAddDialog;

public class WitnessFragment extends BaseFragment implements WitnessView {

    @InjectPresenter
    WitnessFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.witness;
    }

    @Override
    public void createAddMenu() {
        WitnessAddDialog witnessAddDialog = new WitnessAddDialog();
        witnessAddDialog.setView(this);
        witnessAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        witnessAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Witness);
    }


    @Override
    public void createUpdateMenu(int id) {
        WitnessAddDialog witnessAddDialog = new WitnessAddDialog();
        witnessAddDialog.setView(this);
        witnessAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        witnessAddDialog.setUpdateId(id);
        witnessAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Witness);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Witness, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
