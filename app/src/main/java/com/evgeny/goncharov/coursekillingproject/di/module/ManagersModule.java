package com.evgeny.goncharov.coursekillingproject.di.module;

import android.content.Context;

import com.evgeny.goncharov.coursekillingproject.common.managers.MyFragmentManager;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Evgeny Goncharov
 */

@Module(includes = ApplicationModule.class)
public class ManagersModule {

    @Singleton
    @Provides
    MyFragmentManager provideMyFragmentManager() {
        return new MyFragmentManager();
    }


    @Singleton
    @Provides
    DbHelper provideDbHelper(Context context) {
        return new DbHelper(context);
    }

}
