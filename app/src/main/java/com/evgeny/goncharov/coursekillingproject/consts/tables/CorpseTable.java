package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class CorpseTable {

    public static final String NAME_TABLE = "Corpse";
    public static final String ID = "idCorpse";
    public static final String FULL_NAME = "fullName";
    public static final String DATE = "dateOfBirth";
    public static final String CAUSE = "causeOfDeath";
    public static final String ID_EXPERTISE = "idExp";


    public static final String CREATE_CORPSE = "CREATE TABLE " + NAME_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FULL_NAME + " TEXT, "
            + DATE + " TEXT, "
            + CAUSE + " TEXT, "
            + ID_EXPERTISE + " INTEGER, "
            + "FOREIGN KEY (" + ID_EXPERTISE + ") "
            + "REFERENCES " + ExpertiseTable.TABLE_NAME
            + " (" + ExpertiseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";


}
