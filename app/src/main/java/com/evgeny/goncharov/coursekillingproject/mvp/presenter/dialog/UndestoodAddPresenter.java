package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.UnderstoodTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class UndestoodAddPresenter {

    @Inject
    DbHelper helper;


    public UndestoodAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String fullname, String phone, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UnderstoodTable.FULL_NAME, fullname);
        contentValues.put(UnderstoodTable.PHONE, phone);
        contentValues.put(UnderstoodTable.ID_INTERROGATION, id);
        helper.insertItem(UnderstoodTable.TABLE_NAME, contentValues);
    }


    public void update(String fullname, String phone, String idinter, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(UnderstoodTable.FULL_NAME, fullname);
        contentValues.put(UnderstoodTable.PHONE, phone);
        contentValues.put(UnderstoodTable.ID_INTERROGATION, idinter);
        helper.updateItem(UnderstoodTable.TABLE_NAME, contentValues, UnderstoodTable.ID,
                String.valueOf(id));
    }


}
