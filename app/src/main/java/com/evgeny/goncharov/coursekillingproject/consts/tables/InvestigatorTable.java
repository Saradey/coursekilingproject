package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class InvestigatorTable {

    public static final String TABLE_NAME = "Investigator";
    public static final String ID = "idInvestigator";
    public static final String FULL_NAME = "fullName";
    public static final String PASSPORT = "passportNumberSeries";
    public static final String RANK = "rank";
    public static final String DATE = "dateOfBirth";
    public static final String ID_CRIMINAL_CASE = "idCriminalCaseForeign";

    public static final String CREATE_INVESTIGATOR = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FULL_NAME + " TEXT, "
            + PASSPORT + " TEXT, "
            + RANK + " TEXT, "
            + DATE + " TEXT, "
            + ID_CRIMINAL_CASE + " INTEGER, "
            + "FOREIGN KEY (" + ID_CRIMINAL_CASE + ") "
            + "REFERENCES " + CriminalCaseTable.TABLE_NAME
            + " (" + CriminalCaseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";


}
