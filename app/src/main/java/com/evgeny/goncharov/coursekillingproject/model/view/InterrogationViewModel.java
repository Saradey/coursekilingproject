package com.evgeny.goncharov.coursekillingproject.model.view;

public class InterrogationViewModel extends ModelView {


    private int idInterrogation;
    private String description;
    private int idCriminal;

    public int getIdInterrogation() {
        return idInterrogation;
    }

    public void setIdInterrogation(int idInterrogation) {
        this.idInterrogation = idInterrogation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdCriminal() {
        return idCriminal;
    }

    public void setIdCriminal(int idCriminal) {
        this.idCriminal = idCriminal;
    }

    @Override
    public int getId() {
        return idInterrogation;
    }
}
