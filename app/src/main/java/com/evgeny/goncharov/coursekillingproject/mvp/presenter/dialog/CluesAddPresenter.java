package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CluesTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class CluesAddPresenter {

    @Inject
    DbHelper helper;


    public CluesAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String type, String description, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CluesTable.TYPE, type);
        contentValues.put(CluesTable.DESCRIPTION, description);
        contentValues.put(CluesTable.ID_EXPERTISE, id);
        helper.insertItem(CluesTable.NAME_TABLE, contentValues);
    }


    public void update(String type, String description, String id_ex, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CluesTable.TYPE, type);
        contentValues.put(CluesTable.DESCRIPTION, description);
        contentValues.put(CluesTable.ID_EXPERTISE, id_ex);
        helper.updateItem(CluesTable.NAME_TABLE, contentValues, CluesTable.ID,
                String.valueOf(id));
    }

}
