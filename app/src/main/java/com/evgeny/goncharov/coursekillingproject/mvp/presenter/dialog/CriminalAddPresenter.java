package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CriminalTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class CriminalAddPresenter {

    @Inject
    DbHelper helper;


    public CriminalAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String fullname, String charict, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CriminalTable.FULL_NAME, fullname);
        contentValues.put(CriminalTable.CHARECT, charict);
        contentValues.put(CriminalTable.ID_INTERROGATION, id);
        helper.insertItem(CriminalTable.TABLE_NAME, contentValues);
    }


    public void update(String fullname, String charict, String id_inter, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CriminalTable.FULL_NAME, fullname);
        contentValues.put(CriminalTable.CHARECT, charict);
        contentValues.put(CriminalTable.ID_INTERROGATION, id_inter);
        helper.updateItem(CriminalTable.TABLE_NAME, contentValues, CriminalTable.ID,
                String.valueOf(id));
    }

}
