package com.evgeny.goncharov.coursekillingproject.model.view;

public class CrimeSceneViewModel extends ModelView {

    private int idCrimeScene;
    private String address;
    private int idExp;


    public int getIdCrimeScene() {
        return idCrimeScene;
    }

    public void setIdCrimeScene(int idCrimeScene) {
        this.idCrimeScene = idCrimeScene;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIdExp() {
        return idExp;
    }

    public void setIdExp(int idExp) {
        this.idExp = idExp;
    }

    @Override
    public int getId() {
        return idCrimeScene;
    }
}
