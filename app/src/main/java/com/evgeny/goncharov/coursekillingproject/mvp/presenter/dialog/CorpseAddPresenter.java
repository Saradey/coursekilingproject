package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CorpseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class CorpseAddPresenter {

    @Inject
    DbHelper helper;


    public CorpseAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String fullname, String date, String die, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CorpseTable.FULL_NAME, fullname);
        contentValues.put(CorpseTable.DATE, date);
        contentValues.put(CorpseTable.CAUSE, die);
        contentValues.put(CorpseTable.ID_EXPERTISE, id);
        helper.insertItem(CorpseTable.NAME_TABLE, contentValues);
    }


    public void update(String fullname, String date, String die, String id_exp, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CorpseTable.FULL_NAME, fullname);
        contentValues.put(CorpseTable.DATE, date);
        contentValues.put(CorpseTable.CAUSE, die);
        contentValues.put(CorpseTable.ID_EXPERTISE, id_exp);
        helper.updateItem(CorpseTable.NAME_TABLE, contentValues, CorpseTable.ID,
                String.valueOf(id));
    }

}
