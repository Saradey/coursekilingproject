package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class CriminalCaseTable {

    public static final String TABLE_NAME = "CriminalCase";
    public static final String ID = "idCriminalCase";
    public static final String OPENING_DATE = "openingDate";
    public static final String DESCRIPTION = "description";


    public static final String CREATE_CRIMINAL_CASE = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + OPENING_DATE + " TEXT, "
            + DESCRIPTION + " TEXT " + ")";

}
