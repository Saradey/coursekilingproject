package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CrimeSceneFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CrimeSceneView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CrimeSceneAddDialog;


public class CrimeSceneFragment extends BaseFragment implements CrimeSceneView {

    @InjectPresenter
    CrimeSceneFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.crimescene;
    }

    @Override
    public void createAddMenu() {
        CrimeSceneAddDialog crimeSceneAddDialog = new CrimeSceneAddDialog();
        crimeSceneAddDialog.setCluesView(this);
        crimeSceneAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        crimeSceneAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.CrimeScene);
    }

    @Override
    public void createUpdateMenu(int id) {
        CrimeSceneAddDialog crimeSceneAddDialog = new CrimeSceneAddDialog();
        crimeSceneAddDialog.setCluesView(this);
        crimeSceneAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        crimeSceneAddDialog.setUpdateId(id);
        crimeSceneAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.CrimeScene);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.CrimeScene, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
