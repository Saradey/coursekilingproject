package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.CluesFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CluesView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.CluesAddDialog;

public class CluesFragment extends BaseFragment implements CluesView {

    @InjectPresenter
    CluesFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.clues;
    }

    @Override
    public void createAddMenu() {
        CluesAddDialog cluesAddDialog = new CluesAddDialog();
        cluesAddDialog.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        cluesAddDialog.setCluesView(this);
        cluesAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Clues);
    }


    @Override
    public void createUpdateMenu(int id) {
        CluesAddDialog cluesAddDialog = new CluesAddDialog();
        cluesAddDialog.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        cluesAddDialog.setCluesView(this);
        cluesAddDialog.setUpdateId(id);
        cluesAddDialog.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Clues);
    }

    @Override
    public void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Clues, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}