package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class InterrogationTable {

    public static final String TABLE_NAME = "Interrogation";
    public static final String ID = "idInterrogation";
    public static final String DESCRIPTION = "description";
    public static final String ID_CRIMINAL_CASE = "idCriminal";


    public static final String CREATE_INTERROGATION = "CREATE TABLE " + TABLE_NAME + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DESCRIPTION + " TEXT, "
            + ID_CRIMINAL_CASE + " INTEGER, "
            + "FOREIGN KEY (" + ID_CRIMINAL_CASE + ") "
            + "REFERENCES " + CriminalCaseTable.TABLE_NAME
            + " (" + CriminalCaseTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";


}
