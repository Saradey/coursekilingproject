package com.evgeny.goncharov.coursekillingproject.model.view;

public class CriminalCaseViewModel extends ModelView {

    private int idCriminalCase;
    private String openingDate;
    private String description;



    public int getIdCriminalCase() {
        return idCriminalCase;
    }

    public void setIdCriminalCase(int idCriminalCase) {
        this.idCriminalCase = idCriminalCase;
    }

    public String getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getId() {
        return idCriminalCase;
    }
}
