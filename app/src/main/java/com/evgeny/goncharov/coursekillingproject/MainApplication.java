package com.evgeny.goncharov.coursekillingproject;

import android.app.Application;

import com.evgeny.goncharov.coursekillingproject.di.component.ApplicationComponent;
import com.evgeny.goncharov.coursekillingproject.di.component.DaggerApplicationComponent;
import com.evgeny.goncharov.coursekillingproject.di.module.ApplicationModule;

public class MainApplication extends Application {

    private static ApplicationComponent applicationComponent;

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplicationComponent();
    }

    private void initApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

}
