package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.CluesTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.CluesViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.CluesView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

@InjectViewState
public class CluesFragmentPresenter extends MvpPresenter<CluesView> {

    @Inject
    DbHelper dbHelper;

    public CluesFragmentPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }


    public List<ModelView> getDateFromDb() {
        return getListInspection();
    }


    private List<ModelView> getListInspection() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        List<ModelView> listEntity = new ArrayList<>();
        String[] allColumn = {
                CluesTable.TYPE,
                CluesTable.DESCRIPTION,
                CluesTable.ID_EXPERTISE,
                CluesTable.ID
        };

        Cursor cursor = database.query(CluesTable.NAME_TABLE,
                allColumn, null, null, null, null, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ModelView note = cursorTo(cursor);
            listEntity.add(note);
            cursor.moveToNext();
        }

        cursor.close();

        return listEntity;
    }


    private CluesViewModel cursorTo(Cursor cursor) {
        CluesViewModel entity = new CluesViewModel();
        entity.setTypeOfEvidence(cursor.getString(0));
        entity.setDescription(cursor.getString(1));
        entity.setIdExp(cursor.getInt(2));
        entity.setIdClues(cursor.getInt(3));
        return entity;
    }

}
