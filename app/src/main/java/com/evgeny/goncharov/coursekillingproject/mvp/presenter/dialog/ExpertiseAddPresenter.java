package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.ExpertiseTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class ExpertiseAddPresenter {

    @Inject
    DbHelper helper;


    public ExpertiseAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String date, String description, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ExpertiseTable.DATE, date);
        contentValues.put(ExpertiseTable.EXPERT, description);
        contentValues.put(ExpertiseTable.ID_CRIMINAL_CASE, id);
        helper.insertItem(ExpertiseTable.TABLE_NAME, contentValues);
    }


    public void update(String date, String description, String idExp, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ExpertiseTable.DATE, date);
        contentValues.put(ExpertiseTable.EXPERT, description);
        contentValues.put(ExpertiseTable.ID_CRIMINAL_CASE, id);
        helper.updateItem(ExpertiseTable.TABLE_NAME, contentValues, ExpertiseTable.ID,
                String.valueOf(id));
    }

}
