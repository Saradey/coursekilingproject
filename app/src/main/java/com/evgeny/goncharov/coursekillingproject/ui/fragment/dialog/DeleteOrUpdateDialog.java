package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.AdapterListener;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog.DeleteOrUpdatePresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.dialogs.DeleteOrUpdateView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.BaseFragment;

public class DeleteOrUpdateDialog extends DialogFragment implements DeleteOrUpdateView {

    private int idItems;
    private TypeFragment typeFragment;
    private DeleteOrUpdatePresenter presenter = new DeleteOrUpdatePresenter(this);
    private AdapterListener adapter;

    private BaseFragment myFragment;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.delete_or_update)
                .setPositiveButton(R.string.update_update, (dialog, id) -> {
                    presenter.update(idItems, typeFragment);
                }).setNegativeButton(R.string.delete_delete, (dialog, id) -> {
                    presenter.delete(idItems, typeFragment);
                })
                .create();
    }

    public void setBundle(int id, TypeFragment typeFragment, AdapterListener adapter) {
        this.idItems = id;
        this.typeFragment = typeFragment;
        this.adapter = adapter;
    }


    @Override
    public void createUpdateDialog() {
        myFragment.createUpdateMenu(idItems);
    }


    @Override
    public void notifyDataDelete(int idItems) {
        adapter.notifyDataDelete(idItems);
    }

    public void setMyFragment(BaseFragment myFragment) {
        this.myFragment = myFragment;
    }

}
