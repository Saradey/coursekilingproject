package com.evgeny.goncharov.coursekillingproject.mvp.view.activity;

import com.arellomobile.mvp.MvpView;

public interface MainView extends MvpView {

    void goToTheCrimalCaseFragment();

    void goToTheInvestigatorFragment();

    void goToTheExpertiseFragment();

    void goToTheCorpseFragment();

    void goToTheCluesFragment();

    void goToTheCrimeSceneFragment();

    void goToTheInterrogationFragment();

    void goToTheUnderstood();

    void goToTheCriminal();

    void goToTheWitness();

    void openDrawer();

    void createAddDialogMenu();
}
