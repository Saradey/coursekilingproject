package com.evgeny.goncharov.coursekillingproject.consts.tables;

public class WitnessTable {

    public static final String NAME_TABLE = "Witness";
    public static final String ID = "idWitness";
    public static final String FULL_NAME = "fullNameWitness";
    public static final String PHONE = "phoneWitness";
    public static final String ADDRESS = "addressWitness";


    public static final String ID_INTERROGATION = "idInterro";

    public static final String CREATE_WITNESS = "CREATE TABLE " + NAME_TABLE + "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + FULL_NAME + " TEXT, "
            + PHONE + " TEXT, "
            + ADDRESS + " TEXT, "
            + ID_INTERROGATION + " INTEGER, "
            + "FOREIGN KEY (" + ID_INTERROGATION + ") "
            + "REFERENCES " + InterrogationTable.TABLE_NAME
            + " (" + InterrogationTable.ID + ") "
            + "ON DELETE CASCADE ON UPDATE CASCADE"
            + ");";

}
