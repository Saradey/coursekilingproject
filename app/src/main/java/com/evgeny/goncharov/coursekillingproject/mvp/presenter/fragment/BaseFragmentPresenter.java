package com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment;

import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;

import java.util.List;

import javax.inject.Inject;

/**
 * @author Evgeny Goncharov
 */

public abstract class BaseFragmentPresenter {

    @Inject
    DbHelper dbHelper;


    abstract protected List<ModelView> getItem();


}
