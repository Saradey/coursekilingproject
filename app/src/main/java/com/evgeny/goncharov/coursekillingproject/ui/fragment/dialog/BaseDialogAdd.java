package com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog;

import android.support.v4.app.DialogFragment;

public abstract class BaseDialogAdd extends DialogFragment {

    protected int updateOrInsert = -1;    //1 update or 2 insert
    protected int updateId = -1;

    public void setKeyOperation(int keyOperation) {
        this.updateOrInsert = keyOperation;
    }

    public void setUpdateId(int updateId) {
        this.updateId = updateId;
    }
}
