package com.evgeny.goncharov.coursekillingproject.common.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.model.view.CluesViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CorpseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CrimeSceneViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalCaseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.CriminalViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ExpertiseViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.InterrogationViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.InvestigatorViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.ModelView;
import com.evgeny.goncharov.coursekillingproject.model.view.UnderstoodViewModel;
import com.evgeny.goncharov.coursekillingproject.model.view.WitnessViewModel;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.ui.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class BaseAdapter extends RecyclerView.Adapter<BaseViewHolder>
        implements AdapterListener {

    private static List<ModelView> list = new ArrayList<>();
    private TypeFragment typeFragment;
    ListenerForMainActivity listener;


    public BaseAdapter(List<ModelView> list, TypeFragment typeFragment, ListenerForMainActivity listener) {
        this.typeFragment = typeFragment;
        this.list = list;
        this.listener = listener;
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.items_holder, viewGroup, false);
        BaseViewHolder baseViewHolder = new BaseViewHolder(v);
        baseViewHolder.setAdapter(this);
        return baseViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder modelViewBaseViewHolder, int i) {

        switch (typeFragment) {
            case CriminalCase:
                modelViewBaseViewHolder.bindViewHolder((CriminalCaseViewModel) list.get(i));
                break;
            case Clues:
                modelViewBaseViewHolder.bindViewHolder((CluesViewModel) list.get(i));
                break;

            case Corpse:
                modelViewBaseViewHolder.bindViewHolder((CorpseViewModel) list.get(i));
                break;

            case CrimeScene:
                modelViewBaseViewHolder.bindViewHolder((CrimeSceneViewModel) list.get(i));
                break;

            case Criminal:
                modelViewBaseViewHolder.bindViewHolder((CriminalViewModel) list.get(i));
                break;

            case Expertise:
                modelViewBaseViewHolder.bindViewHolder((ExpertiseViewModel) list.get(i));
                break;

            case Investigator:
                modelViewBaseViewHolder.bindViewHolder((InvestigatorViewModel) list.get(i));
                break;

            case Interrogation:
                modelViewBaseViewHolder.bindViewHolder((InterrogationViewModel) list.get(i));
                break;

            case Understood:
                modelViewBaseViewHolder.bindViewHolder((UnderstoodViewModel) list.get(i));
                break;

            case Witness:
                modelViewBaseViewHolder.bindViewHolder((WitnessViewModel) list.get(i));
                break;
        }

    }


    @Override
    public void click(int id) {
        listener.setClick(id, typeFragment, this);
    }


    @Override
    public void notifyDataDelete(int idItems) {
        for (int i = 0; i < getItemCount(); i++) {
            if (list.get(i).getId() == idItems) {
                list.remove(i);
                notifyDataSetChanged();
            }
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


}
