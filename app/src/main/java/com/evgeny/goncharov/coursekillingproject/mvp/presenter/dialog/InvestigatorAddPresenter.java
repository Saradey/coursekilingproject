package com.evgeny.goncharov.coursekillingproject.mvp.presenter.dialog;

import android.content.ContentValues;

import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.consts.tables.InvestigatorTable;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;

import javax.inject.Inject;

public class InvestigatorAddPresenter {

    @Inject
    DbHelper helper;

    public InvestigatorAddPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void insert(String date, String fullname, String serial, String rank, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(InvestigatorTable.DATE, date);
        contentValues.put(InvestigatorTable.FULL_NAME, fullname);
        contentValues.put(InvestigatorTable.PASSPORT, serial);
        contentValues.put(InvestigatorTable.RANK, rank);
        contentValues.put(InvestigatorTable.ID_CRIMINAL_CASE, id);
        helper.insertItem(InvestigatorTable.TABLE_NAME, contentValues);
    }


    public void update(String date, String fullname, String serial, String rank,
                       String id_crimCase, int id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(InvestigatorTable.DATE, date);
        contentValues.put(InvestigatorTable.FULL_NAME, fullname);
        contentValues.put(InvestigatorTable.PASSPORT, serial);
        contentValues.put(InvestigatorTable.RANK, rank);
        contentValues.put(InvestigatorTable.ID_CRIMINAL_CASE, id_crimCase);
        helper.updateItem(InvestigatorTable.TABLE_NAME, contentValues, InvestigatorTable.ID,
                String.valueOf(id));
    }

}