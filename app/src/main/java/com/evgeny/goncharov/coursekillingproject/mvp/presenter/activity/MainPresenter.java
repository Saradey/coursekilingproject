package com.evgeny.goncharov.coursekillingproject.mvp.presenter.activity;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.evgeny.goncharov.coursekillingproject.MainApplication;
import com.evgeny.goncharov.coursekillingproject.model.database.DbHelper;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.MainView;

import javax.inject.Inject;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    DbHelper dbHelper;

    public MainPresenter() {
        MainApplication.getApplicationComponent().inject(this);
    }

    public void goToTheCrimalCaseFragment() {
        getViewState().goToTheCrimalCaseFragment();
    }


    public void goToTheInvestigatorFragment() {
        getViewState().goToTheInvestigatorFragment();
    }


    public void goToTheExpertiseFragment() {
        getViewState().goToTheExpertiseFragment();
    }


    public void goToTheCorpseFragment() {
        getViewState().goToTheCorpseFragment();
    }


    public void goToTheCluesFragment() {
        getViewState().goToTheCluesFragment();
    }


    public void goToTheCrimeSceneFragment() {
        getViewState().goToTheCrimeSceneFragment();
    }


    public void goToTheInterrogationFragment() {
        getViewState().goToTheInterrogationFragment();
    }


    public void goToTheUnderstood() {
        getViewState().goToTheUnderstood();
    }


    public void goToTheCriminal() {
        getViewState().goToTheCriminal();
    }


    public void goToTheWitness() {
        getViewState().goToTheWitness();
    }


    public void clickButtonHome() {
        getViewState().openDrawer();
    }


    public void pressButtonAddItem() {
        getViewState().createAddDialogMenu();
    }
}
