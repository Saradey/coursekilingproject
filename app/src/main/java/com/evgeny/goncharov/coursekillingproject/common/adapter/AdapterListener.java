package com.evgeny.goncharov.coursekillingproject.common.adapter;

public interface AdapterListener {

    void click(int id);

    void notifyDataDelete(int idItems);

}
