package com.evgeny.goncharov.coursekillingproject.ui.fragment;

import android.support.v7.widget.LinearLayoutManager;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.evgeny.goncharov.coursekillingproject.R;
import com.evgeny.goncharov.coursekillingproject.common.adapter.BaseAdapter;
import com.evgeny.goncharov.coursekillingproject.consts.ApiConsts;
import com.evgeny.goncharov.coursekillingproject.consts.TypeFragment;
import com.evgeny.goncharov.coursekillingproject.mvp.presenter.fragment.InterrogationFragmentPresenter;
import com.evgeny.goncharov.coursekillingproject.mvp.view.activity.ListenerForMainActivity;
import com.evgeny.goncharov.coursekillingproject.mvp.view.fragment.InterrogationView;
import com.evgeny.goncharov.coursekillingproject.ui.fragment.dialog.InterrogationAddDialog;

public class InterrogationFragment extends BaseFragment implements InterrogationView {

    @InjectPresenter
    InterrogationFragmentPresenter presenter;

    @Override
    public int getTitleFragment() {
        return R.string.interrogation;
    }

    @Override
    public void createAddMenu() {
        InterrogationAddDialog interrogationFragment = new InterrogationAddDialog();
        interrogationFragment.setView(this);
        interrogationFragment.setKeyOperation(ApiConsts.ADD_ITEM_FLAG);
        interrogationFragment.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Interrogation);
    }

    @Override
    public void createUpdateMenu(int id) {
        InterrogationAddDialog interrogationFragment = new InterrogationAddDialog();
        interrogationFragment.setView(this);
        interrogationFragment.setKeyOperation(ApiConsts.UPDATE_ITEM_FLAG);
        interrogationFragment.setUpdateId(id);
        interrogationFragment.show(
                getActivity().getSupportFragmentManager(), ApiConsts.Interrogation);
    }

    @Override
    protected void inflateList() {
        adapter = new BaseAdapter(presenter.getDateFromDb(), TypeFragment.Interrogation, (ListenerForMainActivity) getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mainList.setLayoutManager(linearLayoutManager);
        mainList.setAdapter(adapter);
    }
}
